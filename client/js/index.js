import './font-awesome.min.js';

import { $_ready, Request, $_, Text } from '@aegis-framework/artemis';

const api = 'http://localhost:3000';

let context;

let disabled = false;


function handleInput (input) {
	if (input !== '') {
		$_('[data-content="chat"]').append(`<div class="row__colum row__column--phone--8 text--right"><div class="input">${input}</div></div>`);
	}
	//input = Text.friendly (input).replace (/-/g, ' ');

	$_('input').value ('');
	const chat = $_('[data-content="chat"]').get (0);
	chat.scrollTop = chat.scrollHeight;
	const conversation = {
		dialog: input
	};

	if (context) {
		conversation.context = context;
	}

	Request.post(`${api}/converse`, conversation, { headers: { 'Content-Type': 'application/json' }}).then(response => response.json()).then((data) => {
		const responses = data.output.generic;
		context = data.context;
		for (const response of responses) {
			if (response.text) {
				$_('[data-content="chat"]').append(`<div class="row__colum row__column--phone--8"><div class="response">${response.text.replace(/(\r\n\t|\n|\r\t)/gm, '<br>')}</div></div>`);
			} else if (response.title && response.options) {
				$_('[data-content="chat"]').append(`
					<div class="row__colum row__column--phone--8">
						<div class="response">${response.title}<br>${response.options.map(opt => `<span data-choice="${opt.value.input.text}"><i class="far fa-circle"></i> ${opt.label}</span>`).join ('<br>')}</div>

					</div>
				`);
			}
		}

		const chat = $_('[data-content="chat"]').get (0);
		chat.scrollTop = chat.scrollHeight;
		disabled = false;
	});
}

// When the page is ready, you can start modifying its DOM!
$_ready(() => {
	handleInput('');
	$_('form').submit ((event) => {
		event.preventDefault();
		if (!disabled) {

			disabled = true;
			const input = $_('input').value ();
			handleInput (input);
		}
	});

	$_('[data-content="chat"]').on ('click', '[data-choice], [data-choice]', function () {
		handleInput ($_(this).data ('choice'));
	});

});